#!/bin/bash

#host
HOST_TESTING=ftp.us.debian.org

#directorio destino
DIR_TESTING=/media/repositorio/testing

#########################
#debian Testing (jessie)#
#########################
debmirror --progress -verbose --debug \
--host=${HOST_TESTING} \
--root=:debian \
--method=rsync \
--dist=jessie \
--section=main,contrib,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_TESTING}