#!/bin/bash

#host
HOST_ESTABLE=ftp.us.debian.org

#directorios de destino
DIR_BACKPORTS=/media/repositorio/backports

################
###Backports####
################
debmirror --progress -verbose --debug \
--host=${HOST_ESTABLE} \
--root=:debian \
--method=rsync \
--dist=wheezy-backports \
--section=main,contrib,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_BACKPORTS}

# deb file:/media/repositorio/backports wheezy-backports main contrib non-free
