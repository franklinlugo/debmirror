#!/bin/bash

#host
HOST_SEGURIDAD=security.debian.org

#directorios de destino
DIR_TESTING_SEGURIDAD=/media/repositorio/testing-seguridad

################
####seguridad###
################
debmirror --progress -verbose --debug \
--host=${HOST_SEGURIDAD} \
--root=:debian-security \
--method=rsync \
--dist=jessie/updates \
--section=main,contrib,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_TESTING_SEGURIDAD}

