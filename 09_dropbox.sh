#!/bin/bash

#host
HOST_DROPBOX=linux.dropbox.com

#directorio destino
DIR_DROPBOX=/media/repositorio/dropbox/

#########################
########Dropbox##########
#########################
debmirror --progress -verbose --debug \
--host=${HOST_DROPBOX} \
--root=:debian \
--method=http \
--dist=wheezy \
--section=main \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_DROPBOX}

#####
# deb http://linux.dropbox.com/debian squeeze main
###To import our GPG keys into your apt repository
# sudo apt-key adv --keyserver pgp.mit.edu --recv-keys 5044912E