#!/bin/bash

#host
HOST_ESTABLE=ftp.us.debian.org

#directorios de destino
DIR_ESTABLE=/media/repositorio/estable

################
#debian estable#
################
debmirror --progress -verbose --debug \
--host=${HOST_ESTABLE} \
--root=:debian \
--method=rsync \
--dist=wheezy \
--section=main,contrib,non-free,main/debian-installer \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_ESTABLE}

# deb file:/media/repositorio/estable wheezy main non-free contrib main/debian-installer
