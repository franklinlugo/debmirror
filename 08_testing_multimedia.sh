#!/bin/bash

#host
HOST_MULTIMEDIA=www.deb-multimedia.org

#directorios de destino
DIR_TESTING_MULTIMEDIA=/media/repositorio/testing-multimedia

################
###Multimedia####
################
debmirror --progress -verbose --debug \
--host=${HOST_MULTIMEDIA} \
--root=:deb \
--method=rsync \
--dist=jessie \
--section=main,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--rsync-options="-aIL --partial" \
--diff=none \
--ignore-small-errors \
${DIR_TESTING_MULTIMEDIA}

##http://www.deb-multimedia.org/debian-m