#!/bin/bash

#host
HOST_MULTIMEDIA=www.deb-multimedia.org

#directorios de destino
DIR_MULTIMEDIA=/media/repositorio/estable-multimedia

################
###Multimedia####
################
debmirror --progress -verbose --debug \
--host=${HOST_MULTIMEDIA} \
--root=:deb \
--method=rsync \
--dist=wheezy \
--section=main,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--rsync-options="-aIL --partial" \
--diff=none \
--ignore-small-errors \
${DIR_MULTIMEDIA}

##http://www.deb-multimedia.org/debian-m
# deb file:/media/repositorio/estable-multimedia wheezy main non-free
