#!/bin/bash

#host
HOST_SEGURIDAD=security.debian.org

#directorios de destino
DIR_SEGURIDAD=/media/repositorio/estable-seguridad

################
####seguridad###
################
debmirror --progress -verbose --debug \
--host=${HOST_SEGURIDAD} \
--root=:debian-security \
--method=rsync \
--dist=wheezy/updates \
--section=main,contrib,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_SEGURIDAD}

# deb file:/media/repositorio/estable-seguridad wheezy/updates main contrib non-free

