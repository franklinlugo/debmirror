#!/bin/bash

#host
HOST_ESTABLE=ftp.us.debian.org

#directorios de destino
DIR_WHEEZY_UPDATES=/media/repositorio/wheezy-updates

#####################
####Wheezy Updates###
#####################
debmirror --progress -verbose --debug \
--host=${HOST_ESTABLE} \
--root=:debian \
--method=rsync \
--dist=wheezy-updates \
--section=main,contrib,non-free \
--arch=i386,amd64 \
--nosource \
--ignore-missing-release \
--no-check-gpg \
--ignore-release-gpg \
--ignore-small-errors \
--diff=none \
--rsync-options="-aIL --partial" \
${DIR_WHEEZY_UPDATES}

# deb file:/media/repositorio/wheezy-updates wheezy-updates main contrib non-free

